function setDarkModeIconActive(active) {
    document.querySelector("#dark-mode-icon").classList = active ? "dark-mode-icon-hidden" : "";
    document.querySelector("#dark-mode-icon-active").classList = active ? "" : "dark-mode-icon-hidden";
}

if (!!localStorage.getItem("darkModeEnabled")) {
    document.body.classList = "dark";
    setDarkModeIconActive(true);
} else {
    localStorage.setItem("darkModeEnabled", "");
}

document.querySelector(".dark-mode-button").addEventListener("click", () => {
    document.body.style.transition = "0.3s";
    const enableDarkMode = !localStorage.getItem("darkModeEnabled");

    if (enableDarkMode) {
        document.body.classList = "dark";
    } else {
        document.body.classList = "";
    }
    
    localStorage.setItem("darkModeEnabled", enableDarkMode ? "1" : "");
    setDarkModeIconActive(enableDarkMode);
});